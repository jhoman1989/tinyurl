﻿Feature: Urls

Background: Database setup
	Given The following users exist in the database
	| Id                                   | FirstName | LastName | Email          |
	| 25A71ADF-F126-4125-8164-8F22E2A17921 | Jordan    | Homan    | j.homan@me.com |

Scenario: Create Url
	When I create the following url 'http://www.google.com/' for the following user '25A71ADF-F126-4125-8164-8F22E2A17921'
	Then The following data should exist in the url table
	| Link                   | UserId                               |
	| http://www.google.com/ | 25A71ADF-F126-4125-8164-8F22E2A17921 |

Scenario: Create Empty Url
	When I create the following url '' for the following user '25A71ADF-F126-4125-8164-8F22E2A17921'
	Then The following exception 'Link cannot be empty.' should have been thrown

Scenario: Create Invalid Url
	When I create the following url 'test123' for the following user '25A71ADF-F126-4125-8164-8F22E2A17921'
	Then The following exception 'Specified link was not a valid url.' should have been thrown

Scenario: Remove Url
	Given The following urls exist in the database
	| Id       | Link                   | UserId                               |
	| G2aPizo1 | http://www.google.com/ | 25A71ADF-F126-4125-8164-8F22E2A17921 |
	When I remove the following url 'G2aPizo1'
	Then The following data should exist in the url table
	| Link | UserId |

Scenario: Remove Invalid Url
	Given The following urls exist in the database
	| Id       | Link                   | UserId                               |
	| G2aPizo1 | http://www.google.com/ | 25A71ADF-F126-4125-8164-8F22E2A17921 |
	When I remove the following url 'G2aPizo6'
	Then The following data should exist in the url table
	| Link                   | UserId                               |
	| http://www.google.com/ | 25A71ADF-F126-4125-8164-8F22E2A17921 |

Scenario: Search Urls
	Given The following urls exist in the database
	| Id       | Link                      | UserId                               |
	| G2aPizo1 | http://www.google.com/    | 25A71ADF-F126-4125-8164-8F22E2A17921 |
	| G2aPizo2 | http://wwww.linkedin.com/ | 25A71ADF-F126-4125-8164-8F22E2A17921 |
	| G2aPizo3 | http://www.yahoo.com/     | 25A71ADF-F126-4125-8164-8F22E2A17921 |
	| G2aPizo4 | http://www.facebook.com/  | 25A71ADF-F126-4125-8164-8F22E2A17921 |
	When I search for the following urls 'google'
	Then The following search results should have been found
	| Id       | Link                      | UserId                               |
	| G2aPizo1 | http://www.google.com/    | 25A71ADF-F126-4125-8164-8F22E2A17921 |

Scenario: Search Urls with empty query
	Given The following urls exist in the database
	| Id       | Link                      | UserId                               |
	| G2aPizo1 | http://www.google.com/    | 25A71ADF-F126-4125-8164-8F22E2A17921 |
	| G2aPizo2 | http://wwww.linkedin.com/ | 25A71ADF-F126-4125-8164-8F22E2A17921 |
	| G2aPizo3 | http://www.yahoo.com/     | 25A71ADF-F126-4125-8164-8F22E2A17921 |
	| G2aPizo4 | http://www.facebook.com/  | 25A71ADF-F126-4125-8164-8F22E2A17921 |
	When I search for the following urls ''
	Then The following search results should have been found
	| Id       | Link                      | UserId                               |