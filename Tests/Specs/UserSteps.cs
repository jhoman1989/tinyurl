﻿using System.Linq;
using Autofac;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using TinyUrl.Database.Tables;
using TinyUrl.Repositories;

namespace Tests.Specs
{
    [Binding]
    public class UserSteps
    {
        [Given(@"The following users exist in the database")]
        public void TheFollowingUsersExistInTheDatabase(Table table)
        {
            var repository = TestContext.AutofacContainer.Resolve<IUserRepository>();
            repository.AddUsers(table.CreateSet<User>().ToList());
        }
    }
}