﻿using Autofac;

namespace Tests.Specs
{
    public class TestContext
    {
        public static ILifetimeScope AutofacContainer { get; set; }
    }
}