﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using TinyUrl.Database.Tables;
using TinyUrl.Repositories;
using TinyUrl.Requests;
using TinyUrl.Services;

namespace Tests.Specs
{
    [Binding]
    public class UrlsSteps
    {
        [When(@"I create the following url '(.*)' for the following user '(.*)'")]
        public void ICreateTheFollowingUrl(string link, Guid userId)
        {
            var service = TestContext.AutofacContainer.Resolve<IUrlService>();
            try
            {
                service.Generate(new CreateUrlRequest {Link = link, UserId = userId});
            }
            catch (InvalidDataException exception)
            {
                ScenarioContext.Current["InvalidDataException"] = exception;
            }
        }

        [When(@"I remove the following url '(.*)'")]
        public void ICreateTheFollowingUrl(string id)
        {
            var service = TestContext.AutofacContainer.Resolve<IUrlService>();
            service.Remove(new RemoveUrlRequest{Id = id});
        }

        [When(@"I search for the following urls '(.*)'")]
        public void ISearchForTheFollowingUrls(string search)
        {
            var service = TestContext.AutofacContainer.Resolve<IUrlService>();
            var results = service.Search(new SearchUrlRequest{ Search = search });
            ScenarioContext.Current["SearchResults"] = results;
        }

        [Given(@"The following urls exist in the database")]
        public void TheFollowingUsersExistInTheDatabase(Table table)
        {
            var repository = TestContext.AutofacContainer.Resolve<IUrlRepository>();
            repository.AddUrls(table.CreateSet<Url>().ToList());
        }

        [Then(@"The following data should exist in the url table")]
        public void TheFollowingDataShouldExistInTheUrlTable(Table table)
        {
            var repo = TestContext.AutofacContainer.Resolve<IUrlRepository>();
            table.CompareToSet(repo.Get());
        }

        [Then(@"The following search results should have been found")]
        public void ThenTheFollowingSearchResultsShouldHaveBeenFound(Table table)
        {
            table.CompareToSet((IEnumerable<Url>)ScenarioContext.Current["SearchResults"]);
        }

        [Then(@"The following exception '(.*)' should have been thrown")]
        public void ThenTheFollowingSearchResultsShouldHaveBeenFound(string exception)
        {
            var dataException = (InvalidDataException) ScenarioContext.Current["InvalidDataException"];
            Assert.AreEqual(dataException.Message, exception);
        }
    }
}
