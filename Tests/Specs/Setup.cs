﻿using Autofac;
using TechTalk.SpecFlow;
using TinyUrl;
using TinyUrl.Database;

namespace Tests.Specs
{
    [Binding]
    public class Setup
    {
        [BeforeScenario]
        public void Initialize()
        {
            var container = ContainerConfig.RegisterDependencies();
            TestContext.AutofacContainer = container;

            var context = TestContext.AutofacContainer.Resolve<TinyUrlContext>();
            context.Database.ExecuteSqlCommand("Delete from [Url]");
            context.Database.ExecuteSqlCommand("Delete from [User]");
        }
    }
}