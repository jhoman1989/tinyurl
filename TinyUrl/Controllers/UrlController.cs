﻿using System.Collections.Generic;
using System.Web.Http;
using TinyUrl.Database.Tables;
using TinyUrl.Requests;
using TinyUrl.Services;

namespace TinyUrl.Controllers
{
    public class UrlController : ApiController
    {
        private readonly IUrlService _urlService;

        public UrlController(IUrlService urlService)
        {
            _urlService = urlService;
        }

        [HttpPost]
        public Url Post([FromBody]CreateUrlRequest request)
        {
            return _urlService.Generate(request);
        }

        [HttpGet]
        public Url Get(string id)
        {
            return _urlService.GetById(id);
        }

        [HttpGet]
        [Route("~/api/Search/{id}")]
        public IEnumerable<Url> Search([FromUri] string id)
        {
            var results = _urlService.Search(new SearchUrlRequest {Search = id});
            return results;
        }
        
        [HttpDelete]
        public void Delete(string id)
        {
            _urlService.Remove(new RemoveUrlRequest {Id = id});
        }
    }
}