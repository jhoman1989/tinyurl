﻿using System.Web.Mvc;

namespace TinyUrl.Controllers
{
    public class SearchController : Controller
    {
        public dynamic Index(string id)
        {
            ViewBag.Title = "Search Results";
            ViewBag.Search = id;
            return View("Search");
        }
    }
}