﻿using System.Web.Mvc;
using TinyUrl.Requests;
using TinyUrl.Services;

namespace TinyUrl.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUrlService _urlService;
        private const string PageNotFound = "404";

        public HomeController(IUrlService urlService)
        {
            _urlService = urlService;
        }

        public dynamic Index(string id)
        {
            ViewBag.Title = "Home Page";
            if (string.IsNullOrEmpty(id))
            {
                return View();
            }
            if (id == PageNotFound)
            {
                ViewBag.Title = "Page Not Found";
                return View(PageNotFound);
            }
            return Redirect(_urlService.Redirect(new RedirectRequest { Id = id }));
        }
    }
}
