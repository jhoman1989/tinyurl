﻿namespace TinyUrl.Requests
{
    public class RemoveUrlRequest
    {
        public string Id { get; set; }
    }
}