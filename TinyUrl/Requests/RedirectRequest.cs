﻿namespace TinyUrl.Requests
{
    public class RedirectRequest
    {
        public string Id { get; set; }
    }
}