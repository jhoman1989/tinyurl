﻿using System;

namespace TinyUrl.Requests
{
    public class CreateUrlRequest
    {
        public string Link { get; set; }

        public Guid UserId { get; set; }
    }
}