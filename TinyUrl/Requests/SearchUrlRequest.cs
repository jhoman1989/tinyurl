﻿namespace TinyUrl.Requests
{
    public class SearchUrlRequest
    {
        public string Search { get; set; }
    }
}