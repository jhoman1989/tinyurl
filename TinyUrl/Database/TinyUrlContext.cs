﻿using System.Data.Entity;
using TinyUrl.Database.Tables;

namespace TinyUrl.Database
{
    public class TinyUrlContext : DbContext
    {
        public TinyUrlContext() : base("TinyUrlContext")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new DbUrlConfiguration());
            modelBuilder.Configurations.Add(new DbUserConfiguration());
        }

        public DbSet<Url> Urls { get; set; }
        public DbSet<User> Users { get; set; }
    }
}