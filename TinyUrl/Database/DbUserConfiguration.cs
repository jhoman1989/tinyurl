﻿using System.Data.Entity.ModelConfiguration;
using TinyUrl.Database.Tables;

namespace TinyUrl.Database
{
    public class DbUserConfiguration : EntityTypeConfiguration<User>
    {
        public DbUserConfiguration()
        {
            ToTable("User");
            HasKey(d => d.Id);   
        }
    }
}