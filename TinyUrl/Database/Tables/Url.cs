using System;
using System.ComponentModel.DataAnnotations;

namespace TinyUrl.Database.Tables
{
    public class Url
    {
        [Key]
        public string Id { get; set; }
        public string Link { get; set; }
        public Guid UserId { get; set; }
        public virtual User User { get; set; }


        public string ShortLink
        {
            get { return "http://localhost:17681/" + Id; }
        }
    }
}