﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TinyUrl.Database.Tables
{
    public class User
    {
        [Key]
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual ICollection<Url> Urls { get; set; }
    }
}