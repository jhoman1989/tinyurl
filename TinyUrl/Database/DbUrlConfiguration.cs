﻿using System.Data.Entity.ModelConfiguration;
using TinyUrl.Database.Tables;

namespace TinyUrl.Database
{
    public class DbUrlConfiguration : EntityTypeConfiguration<Url>
    {
        public DbUrlConfiguration()
        {
            ToTable("Url");
            HasKey(d => d.Id);
            HasRequired(i => i.User).WithMany(i => i.Urls).HasForeignKey(i => i.UserId);
        }
    }
}