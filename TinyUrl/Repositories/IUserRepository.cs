﻿using System;
using System.Collections.Generic;
using TinyUrl.Database.Tables;

namespace TinyUrl.Repositories
{
    public interface IUserRepository
    {
        User GetById(Guid id);
        void AddUsers(IList<User> users);
        void Add(User user);
    }
}