﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TinyUrl.Database;
using TinyUrl.Database.Tables;

namespace TinyUrl.Repositories
{
    public class UrlRepository : IUrlRepository
    {
        private readonly TinyUrlContext _context;

        public UrlRepository(TinyUrlContext context)
        {
            _context = context;
        }

        public IEnumerable<Url> Get()
        {
            return _context.Urls;
        } 

        public Url GetById(string id)
        {
            return _context.Urls.SingleOrDefault(t => t.Id == id);
        }

        public void Add(Url url)
        {
            AddUrls(new[] {url});
        }

        public void AddUrls(IList<Url> urls)
        {
            _context.Urls.AddRange(urls);
            _context.SaveChanges();
        }

        public void Remove(string id)
        {
            var url = GetById(id);
            if (url == null)
            {
                return;
            }
            _context.Urls.Remove(url);
            _context.SaveChanges();
        }

        public IEnumerable<Url> Search(string search)
        {
            return _context.Urls.Where(t => t.Link.Contains(search)).Include(t=> t.User).ToList();
        } 
    }
}