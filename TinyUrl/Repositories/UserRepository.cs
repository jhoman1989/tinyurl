﻿using System;
using System.Collections.Generic;
using System.Linq;
using TinyUrl.Database;
using TinyUrl.Database.Tables;

namespace TinyUrl.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly TinyUrlContext _context;

        public UserRepository(TinyUrlContext context)
        {
            _context = context;
        }

        public User GetById(Guid id)
        {
            return _context.Users.SingleOrDefault(u => u.Id == id);
        }

        public void Add(User user)
        {
            AddUsers(new [] {user});
        }

        public void AddUsers(IList<User> users)
        {
            _context.Users.AddRange(users);
            _context.SaveChanges();
        }
    }
}