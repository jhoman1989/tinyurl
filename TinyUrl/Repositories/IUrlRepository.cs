﻿using System.Collections.Generic;
using TinyUrl.Database.Tables;

namespace TinyUrl.Repositories
{
    public interface IUrlRepository
    {
        IEnumerable<Url> Get();
        Url GetById(string id);
        void Add(Url url);
        void AddUrls(IList<Url> urls);
        void Remove(string id);

        IEnumerable<Url> Search(string search);
    }
}