﻿using System;
using System.Collections.Generic;
using TinyUrl.Requests;

namespace TinyUrl.Validators
{
    public class UrlRequestValidator
    {
        private readonly CreateUrlRequest _request;

        public UrlRequestValidator(CreateUrlRequest request)
        {
            _request = request;
        }

        public List<string> Validate()
        {
            var errors = new List<string>();
            if (string.IsNullOrEmpty(_request.Link))
            {
                errors.Add("Link cannot be empty.");
            }
            else if (!IsValidUrl(_request.Link))
            {
                errors.Add("Specified link was not a valid url.");
            }
            return errors;
        }

        private bool IsValidUrl(string link)
        {
            Uri uriResult;
            return Uri.TryCreate(link, UriKind.Absolute, out uriResult)
                   && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }
    }
}