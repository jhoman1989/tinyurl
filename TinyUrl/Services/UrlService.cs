﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using TinyUrl.Database.Tables;
using TinyUrl.Repositories;
using TinyUrl.Requests;
using TinyUrl.Validators;

namespace TinyUrl.Services
{
    public class UrlService : IUrlService
    {
        private readonly IUrlRepository _urlRepository;

        public UrlService(IUrlRepository urlRepository)
        {
            _urlRepository = urlRepository;
        }

        public Url Generate(CreateUrlRequest request)
        {
            var validatinResult = new UrlRequestValidator(request).Validate();
            if (validatinResult.Any())
            {
                throw new InvalidDataException(validatinResult.First());
            }
            var url = new Url {
                Id = UniqueIdentifier.New(), 
                Link = request.Link,
                UserId = request.UserId
            };
            _urlRepository.Add(url);
            return url;
        }

        public string Redirect(RedirectRequest request)
        {
            var url = _urlRepository.GetById(request.Id);
            if (url == null)
            {
                return "404";
            }
            return url.Link;
        }

        public Url GetById(string id)
        {
            return _urlRepository.GetById(id);
        }

        public void Remove(RemoveUrlRequest request)
        {
            _urlRepository.Remove(request.Id);
        }

        public IEnumerable<Url> Search(SearchUrlRequest request)
        {
            if (request == null || string.IsNullOrEmpty(request.Search))
            {
                return new List<Url>();
            }
            request.Search = request.Search.Replace(" ", "");
            if (request.Search.Length == 0)
            {
                return new List<Url>();
            }
            return _urlRepository.Search(request.Search);
        }
    }

    class UniqueIdentifier
    {
        public static string New()
        {
            var maxSize = 8;
            var chars = new char[62];
            var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();
            var size = maxSize;
            var data = new byte[1];
            var crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            var result = new StringBuilder(size);
            foreach (var b in data)  {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return result.ToString();
        }
    }
}