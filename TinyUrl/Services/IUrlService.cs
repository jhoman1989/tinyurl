﻿using System.Collections.Generic;
using TinyUrl.Requests;
using Url = TinyUrl.Database.Tables.Url;

namespace TinyUrl.Services
{
    public interface IUrlService
    {
        Url Generate(CreateUrlRequest request);
        Url GetById(string id);
        void Remove(RemoveUrlRequest request);
        IEnumerable<Url> Search(SearchUrlRequest request);

        string Redirect(RedirectRequest request);
    }
}